package cz.uhk.mois.endor.bankapi.model.commons;

public record Value(
        Integer amount,
        String currency
) {}